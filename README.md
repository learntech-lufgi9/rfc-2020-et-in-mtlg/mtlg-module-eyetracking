# Eyetracking Module

This MTLG component communicates with the pupils core eye-tracking glasses. It provides the possibility to react on gazes of the players.

# Demo
There are two demos. One [smaller demo](https://git.rwth-aachen.de/learntech-lufgi9/rfc-2020-et-in-mtlg/mtlg-eyetracking-test-game) where the gazes are visualized and an area and a listener on an object are defined. The other demo is the [Regex Demo Game](https://git.rwth-aachen.de/learntech-lufgi9/rfc-2020-et-in-mtlg/regex3-example) where objects in the middle are sorted into areas.

# Requirements
The module requires the newest version [utilities module](https://git.rwth-aachen.de/learntech-lufgi9/rfc-2020-et-in-mtlg/mtlg-modul-utilities) (it needs apriltags).
Additionally, the hardware (pupil eyetracking glasses) and the corresponding software is required. See [Pupil Capture](https://pupil-labs.com/products/core/) and [RFC implementation](https://git.rwth-aachen.de/learntech-lufgi9/rfc-2020-et-in-mtlg/rfc-implementation).

# Use the Module
The module must be included in the game ([explained here](https://gitlab.com/mtlg-framework/mtlg-gameframe/-/wikis/mtlg/modul-%C3%BCbersicht)).

## Initialization
For starting the module `MTLG.eyetracking.initialize(configured = null)` must be called. This will start the connection to the RFC implementation.
The configuration object can contain the following attributes:
```javascript
var defaults = {
  tagfamiliy: "tag36h11", // family of apriltags
  width: 150, // default width of apriltag
  height: 150, // default height of apriltag
  gazePrecision: 20, // precision of the gaze of a player (mostly for detecting gameObjects etc.)
  confidence: 0.8, // minimal confidence (TODO: must be sent to RFC implementation)
  visualizeGazeOfPlayer: false, // true when the gaze should be visualized
  createTags: true // false when initialize should not define a surface with tags
}
```
After the normal registering of the players, the players must be initialized for the eyetracking module so that the glasses are mapped to the right players. This is done by calling `MTLG.eyetracking.initPlayers()`. This starts the mapping process. A rectangle is displayed in the middle which the first player should fixate. When the player is recognized another rectangle is displayed. For each registered player one.

## Visualization
Optionally, the gaze of the players or a single player can be visualized with the following functions:
```javascript
MTLG.eyetracking.visualizeGazeOfAllPlayers()
MTLG.eyetracking.visualizeGazeOfPlayer(playerID, color)
```
This can also be set in the configuration.

## Surfaces
Gaze information are only received when they are on an recognized surface. The pupil glasses can recognize a surface with apriltags. With the function `createAreaWithTags` six apriltags are displayed at the border of the surface. The surface with its apriltags must be registered in Pupil Capture like described [here](https://docs.pupil-labs.com/core/software/pupil-capture/#defining-a-surface).
```javascript
createAreaWithTags(name, x, y, width, height, stage, options)
```
 the function has seven parameters. The `name` must be the same as defined in Pupil Capture. `x, y, width, height` are the coordinates and size of the surface rectangle. `stage` is the container on which the apriltags should be added as children. With `options` the height and width of the apriltags can be specified.

## Areas
Rectangle areas can be defined. When someone looks at the area this is documented in the database. Additionally, you can register a listener to that area so that the listener is called whenever someone looks at the area. This way you can react in real-time.
 ```javascript
createArea(name, x, y, width, height, surface = null)
 ```
Each area has its own `name` and like the surfaces coordniates and size of the rectangle defined by `x, y, width, height`. An area is always belonging to on surface, this is specifed with the name of the surface. If no name is given the areas automatically belongs to the main surface called 'surface'.

## GameObjects
All gameObjects can be tracked. Every tracked object is saved in the database when someone looks at it. In order to make an gameObject tracked the following code must be executed:
```javascript
var trackedObject = new TrackedObject(gameObject);
```
where `gameObject` is the gameObject that should be tracked.
You can register a listener to a gameObject so that everytime when the object is watched the listener gets called. You can either register a trackedObject or a gameObject that is not already tracked, then a trackedObject is generated.

## Register Listener
Listener can be registered for all events so that you get all gaze events from the RFC implementation, for single objects or all objects at once and for areas.
When registering for all events you just give the callback that should be called.
When registering for objects or areas the object or the name of the area must be given and the listener function that should be called. Additionally, a `min_duration` can be given. When this is 0, immediately when the object/area is watched the listener gets called. When it is >0 is waits these milliseconds and only if the player fixates the object/area for this long the listener gets called.  
The last parameter is `update_interval`. When it is 0 the listener gets called for every incoming event as long as the player looks at it. When >0 it waits these milliseconds before calling the listener the next time.
```javascript

subscribeToAllEvents(listener)
registerListenerForObject(obj, listener, min_duration = 0, update_interval = 0)
registerListenerForAllObjects(listener, min_duration = 0, update_interval = 0)
registerListenerForArea(area, listener, min_duration = 0, update_interval = 0)
```

# Overview of functions
- `createSurface(name, x, y, width, height, stage, options)`: ,
- `createArea(name, x, y, width, height, surface = null)`: ,
- `removeArea(name)`: ,
- `initialize(configuration = null)`: ,
- `initPlayers()`: starts the initialization process for registering the glasses to the players,
- `getGazeOfPlayer(playerID)`: returns an object with coordinates and gameobject if present,
- `getGazes()`: returns an array of the gaze of all players (see `getGazeOfPlayer`),
- `getAllWatchedObjects()`: returns an array of all objects that have been watched in this session,
- `subscribeToAllEvents(listener)`: registers listener to all gaze events,
- `registerListenerForObject(obj, cb, dur = 0, up = 0)`: registers listener to one object,
- `registerListenerForAllObjects(cb, dur = 0, up = 0)`: registers listener to all objects,
- `registerListenerForArea(area, db, dur = 0, up = 0)`: registers listener to one area,
- `removeListenerForObject(trackId)`: removes the listener for the object with the trackId,
- `removeListenerForAllObjects(id)`: removes listener for all objects, the id is return when registering,
- `removeListenerForArea(id)`: removes listener for area, the id is return when registering,
- `unsubscribeFromAllEvents(id)`: removes listener for all events, the id is return when registering,
- `TrackedObject`: Object class for objects that should be tracked and saved in DB,
- `trackedObjects`: Map of all trackedObjects,
- `visualizeGazeOfPlayer(playerID, color)`: visualize the gaze of the player with playerID in the given color,
- `visualizeGazeOfAllPlayers`: visualizes the gaze of all players
