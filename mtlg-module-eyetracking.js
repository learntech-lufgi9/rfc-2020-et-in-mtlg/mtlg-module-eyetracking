var init = function (options, callback) {

};

var defaults = {
  tagfamiliy: "tag36h11",
  width: 150,
  height: 150,
  gazePrecision: 20,
  confidence: 0.8,
  visualizeGazeOfPlayer: false,
  createTags: true
}
var session_id = null;
var timeInitializedAt;
var connected = false;
var subscribed = false;

var usedTags = [];
var areas = []; //{name: "", x: 0, y: 0, width: 100, height: 100, surface: ""}
var surfaces = []; //{name: "", x: 0, y: 0, width: 100, height: 100}

// Saves the last gaze data of all 4 players
var players = new Map();
var playersIDs = new Map();
var playersInitialized = false;

// Listeners
var listenersForAreas = [];
var listenersForObjects = new Map();
var listenersForAllObjects = [];
var listenersForAllEvents = [];

var listenerForAreaID = 0;
var listenerForObjectID = 0;
var listenerForAllObjectsID = 0;
var listenerAllEventsID = 0;

var eyetracking_stage;

var colors = ['#ff0000', '#00ff00', '#0000ff', '#ffdd00'];

var trackedObjects = new Map();


/**
 * initialize - This function is to start the eyetracking module. It starts the
 * connection to the eyetracking glasses and
 *
 * @param  {Object} configuration = null The module can be configured with an object like:
 * {
 *   tagfamiliy: "tag36h11",
 *   width: 150,
 *   height: 150,
 *   gazePrecision: 20,
 *   confidence: 0.8,
 *   visualizeGazeOfPlayer: false,
 *   createTags: true
 * }
 */
function initialize(configuration = null){
  // override defaults with custom configuration
  defaults = Object.assign(defaults, configuration);

  // set session_id
  session_id = uuidv4();
  // create game surface area for tracking
  var options = MTLG.getOptions();

  eyetracking_stage = addEyetrackingStage();
  resizeEyetrackingStage();
  if (defaults.createTags) createAreaWithTags("surface", 0, 0, options.width, options.height, eyetracking_stage);

  var host = options.eyetracking && options.eyetracking.host ? options.eyetracking.host : "localhost";
  var port = options.eyetracking && options.eyetracking.port ? options.eyetracking.port : 5000;

  // connect to pupils core master
  initWebsocket(host, port);
};


/**
 * addEyetrackingStage - This function adds a new canvas layer for the apriltags
 * and optional visualization of the gaze of players. This is done on a new #
 * canvas layer which lies on top of the game canvas so that the tags are
 * always visible.
 *
 * @return {Stage}  returns the newly create stage of the new eyetracking canvas
 */
function addEyetrackingStage() {
  let eyetracking_stage_canvas = MTLG.getStage().canvas.cloneNode(true);
  eyetracking_stage_canvas.setAttribute('id', 'eyetracking');
  MTLG.getStage().canvas.parentNode.insertBefore(eyetracking_stage_canvas, MTLG.getStage().canvas.nextSibling);

  var stage = new createjs.Stage(eyetracking_stage_canvas);
  stage.nextStage = MTLG.getStage();
  createjs.Ticker.addEventListener("tick", stage);
  window.addEventListener('resize', resizeEyetrackingStage, false);
  return stage;
}


/**
 * resizeEyetrackingStage - Helper function for resizing the eyetracking stage
 *
 */
function resizeEyetrackingStage() {
  var canvas = eyetracking_stage.canvas;
  let scaleX = window.innerWidth / MTLG.getOptions().width;
  let scaleY = window.innerHeight / MTLG.getOptions().height;
  if (scaleX < scaleY) {
    canvas.width = window.innerWidth;
    canvas.height = MTLG.getOptions().height * scaleX;
  } else {
    canvas.width = MTLG.getOptions().width * scaleY;
    canvas.height = window.innerHeight;
  }
  var scale = canvas.width / MTLG.getOptions().width;
  eyetracking_stage.scaleX = scale;
  eyetracking_stage.scaleY = scale;

  eyetracking_stage.update();
}


/**
 * uuidv4 - Generates a new uuidv4.
 *
 * @return {UUID string}  returns a uuid string in form of
 * xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
 */
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}


/**
 * initWebsocket - Initializes the connection to the websocket.
 * When connected commands to set time to 0 and activate fixation detector are
 * sent. As well the configured surfaces and areas.
 * Messages that this socket can receive are 'log' and 'msg_py'.
 *
 * @param  {type} host description
 * @param  {type} port description
 * @return {type}      description
 */
function initWebsocket(host, port) {
    config = host + ":" + port + "/";

    socket = io(config);// Connect with server

    socket.on('connect', function(message) {
      console.log('I am conntected');
      connected = true;
      // TODO: set time Crashes the fixation detector
      sendCMD("time set", "0");
      timeInitializedAt = Date.now();
      console.log('Time set to 0 at', timeInitializedAt);

      sendSession(session_id, timeInitializedAt);

	  setTimeout(function() {
		sendCMD("activate", "Fixation_Detector");
	  }, 500);


      subscribeToEyetracking();
      sendAllSurfaces();
      sendAllAreas();
    })
    // Received text to log
    socket.on('log', function (message) {
      console.log("log", message);
	    socket.emit('msg_js', {message: "Hello from js"});
    });

  socket.on('msg_py', function (message) {
    if (message.cmd) {
      if (message.cmd.startsWith("screenspace") || message.cmd.startsWith("area") || message.cmd.startsWith("clients list")) {
        console.log(message.response);
      }
    } else if (message.event && subscribed) {
      //console.log('event:', message.event);


      handleEvent(message.event);
    }

  });
}


/**
 * initPlayers - This function initializes the number of players defined in
 * the device manifest file. For the initialization the players have to look at
 * a box in the middle of the gamefield one after another.
 *
 */
function initPlayers() {
  var numberOfPlayers = MTLG.getPlayerNumber();

  var st_container = new createjs.Container()
  st_container.setBounds(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
  MTLG.getStageContainer().addChild(st_container)
  buildBackground(st_container);
  players.clear();
  playersIDs.clear();
  initPlayer(0, numberOfPlayers, st_container);
}


/**
 * initPlayer - Helper function to initialize the players. Constructs the
 * initialization process and maps the playerId and pupil glasses id.
 *
 * @param  {type} id                MTLG id of player
 * @param  {type} numberPlayersLeft number of players left to initialize
 * @param  {type} container         description
 * @return {type}                   description
 */
function initPlayer(id, numberPlayersLeft, container) {
  var name = MTLG.getPlayerName(id);
  var shape = buildCalibrationObject(colors[id], container);
  var txt = buildTextObject(id, container);
  // register listener so that gaze events are received when a player
  // looks at the object.
  var listenerID = registerListenerForObject(shape, (event) => {
    if (!Array.from(playersIDs.values()).includes(event.player)){
      removeListenerForObject(listenerID);
      shape.parent.removeChild(shape);
      txt.parent.removeChild(txt);

      players.set(event.player, {
        id: id,
        pupilMasterId: event.player,
        coord: null,
        time: null,
        confidence: null
      });
      playersIDs.set(id,event.player);
      console.log('Player '+id+' registered with '+event.player );

      if(defaults.visualizeGazeOfPlayer) visualizeGazeOfPlayer(id, colors[id]);

      numberPlayersLeft--;
      if(numberPlayersLeft > 0) {
        id += 1;
        setTimeout(() => {
          initPlayer(id, numberPlayersLeft, container);
        },500);

      } else {
        playersInitialized = true;
	      MTLG.getStageContainer().removeChild(container)
      }
    }else{
      console.log(event.player+'already registered');
    }
  }, 500, 0, false);
}


/**
 * buildCalibrationObject - Helperfunction
 */
function buildCalibrationObject(color, container) {
  var shape = new createjs.Shape();
  var width = 300;
  var height = 300;
  shape.graphics.setStrokeStyle(1).beginStroke('#000000').beginFill(color).drawRect(0, 0, width, height);
  shape.x = MTLG.getOptions().width * 1/2 - width/2;
  shape.y = MTLG.getOptions().height * 1/2 - height/2;
  shape.setBounds(0, 0, width, height);
  container.addChild(shape);

  return shape;
}

/**
 * buildTextObject - Helperfunction
 *
 */
function buildTextObject(id, container){
	var txt = new createjs.Text("Player "+(id+1)+" please look here","25px Arial");
	txt.textAlign = 'center';
    txt.textBaseline = 'middle';
	txt.x = MTLG.getOptions().width * 1/2;
	txt.y = MTLG.getOptions().height * 1/2 - 175;
	container.addChild(txt);

	return txt

}


/**
 * buildBackground - Helperfunction
 *
 */
function buildBackground(container){
  var shape = new createjs.Shape();
  var width = MTLG.getOptions().width;
  var height = MTLG.getOptions().height;
  shape.graphics.beginFill('white').drawRect(0, 0, width, height);
  shape.setBounds(0, 0, width, height);
  container.addChild(shape);
}


/**
 * handleEvent - Handles the incoming gaze events.
 * It detects if the gaze collides with objects or areas, if so it triggers
 * the listeners and saves the objects in db
 *
 * @param  {type} event description
 * @return {type}       description
 */
async function handleEvent(event) {

  if(event.gazes && event.gazes.length > 0) {
    var time_mtlg = Date.now();
    var screenposition = event.gazes[event.gazes.length - 1].screen_pos;
    var coordinates = {
      x: screenposition[0],
      y: screenposition[1]
    }

    if (playersInitialized) {
      var playerID = event.player;
      if (players.get(playerID)) {
        players.get(playerID).coord = coordinates;
      }
    }

    listenersForAllEvents.forEach((listener, _) => {
      listener.callback(event);
    });
    // Detect collision with areas
    event.areas.forEach((area, _) => {
      listenersForAreas.forEach((listener, _) => {
        if (listener.area == area) {
          if (shouldTriggerListener(listener, event)) {
            listener.last_visit.set(event.player, Date.now());
            listener.callback(event);
          }
        }
      });
    });

    // Detect collision with gameObjects
    var objects = detectCollisions(coordinates);
    if(objects.length > 0) {
      listenersForAllObjects.forEach((listener, _) => {
        if (shouldTriggerListener(listener, event)) {
          listener.last_visit.set(event.player, Date.now());
          listener.callback(event, objects);
        }
      });
    }
    objects.forEach((obj, _) => {
      obj.last_seen = {
        player: event.player,
        time: time_mtlg
      }
      var sendable = obj.toSendables();
      sendable = sendable[obj.trackId];
      var send = {
        createjs_id: sendable.createJsObject.id,
        track_id: sendable.createJsObject.trackId,
        session_id: session_id,
        time_pupil: event.time,
        time_mtlg: time_mtlg,
        name: sendable.createJsObject.name,
        pos_x: obj.createJsObject.localToGlobal(0,0).x / MTLG.getStageContainer().scaleX,
        pos_y: obj.createJsObject.localToGlobal(0,0).y / MTLG.getStageContainer().scaleY,
        parent: sendable.parent
      }
      socket.emit('save_db', send);

      let listener = listenersForObjects.get(obj.trackId);
      if (listener && shouldTriggerListener(listener, event)) {
        listener.last_visit.set(event.player, Date.now());

        listener.callback(event);
      }
    });
  }
}


/**
 * sendSession - sends sessionid to pupil master
 *
 * @param  {type} session_id              session_id
 * @param  {type} time                    MTLG time
 * @param  {type} description = "default"
 */
function sendSession(session_id, time, description = "default") {
  socket.emit('session_id', {session_id: session_id, time: time, description: description})
}


/**
 * sendCMD - generic function to send a command to pupil master
 *
 * @param  {string}  cmd   command
 * @param  {string} [null] optional number of parameters of strings which will
 *  be appended.
 */
function sendCMD(cmd) {
  for(var i = 1; i < arguments.length; i++) {
    cmd += " " + arguments[i];
  }
  socket.emit('msg_js', {cmd: cmd});
}


/**
 * subscribeToEyetracking - subscribes to pupil core to receive all gaze data.
 *
 */
function subscribeToEyetracking() {
  sendCMD("subscribe");
  subscribed = true;
}


/**
 * unsubscribeToEyetracking - unsubscribes to all pupil core gaze messages
 *
 */
function unsubscribeToEyetracking() {
  sendCMD("unsubscribe");
  subscribed = false;
}


/**
 * addSurface - sends an area to pupil core
 *
 * @param  {type} item area
 */
function addSurface(item) {
  sendCMD("area", "add " + item.name + " " + item.x + " " + item.y + " " + (item.x + item.width) + " " + (item.y + item.height));
}


/**
 * sendScreenspace - sends the area called game_surface to pupil core
 *
 */
function sendScreenspace() {
  areas.forEach((item, i) => {
    if (item.name === "game_surface") {
      sendCMD("screenspace", item.x + " " + item.y + " " + item.width + " " + item.height);
      return true;
    }
  });
}


/**
 * removeSurface - removes locally and for pupil master the surface with the name
 *
 * @param  {type} name name of surface
 */
function removeSurface(name) {
  let index = -1;
  for (let i = 0; i < surfaces.length; i++) {
    if (item.name === name) {
      sendCMD("surface", "remove" + item.name);
      index = i;
      break;
    }
  }
  surfaces.splice(index, 1);;
}


/**
 * sendSurface - sends the surface with the given name to pupil master
 *
 * @param  {type} name name of surface
 */
function sendSurface(name) {
  surfaces.forEach((item, i) => {
    if (item.name === name) {
      sendCMD("surface", "add " + item.name + " " + item.x + " " + item.y + " " + (item.x + item.width) + " " + (item.y + item.height));
      return true;
    }
  });
}


/**
 * sendAllSurfaces - sends all added surfaces to pupil master
 *
 */
function sendAllSurfaces() {
  surfaces.forEach((item, i) => {
    sendCMD("surface", "add " + item.name + " " + item.x + " " + item.y + " " + (item.x + item.width) + " " + (item.y + item.height));
  });
}


/**
 * removeArea - removes the area with the given name locally and at pupil master
 *
 * @param  {type} name name of area
 */
function removeArea(name) {
  let index = -1;
  for (let i = 0; i < areas.length; i++) {
    if (item.name === name) {
      sendCMD("area", "remove" + item.name);
      index = i;
      break;
    }
  }
  areas.splice(index, 1);
}


/**
 * sendArea - sends the area with the given name to pupil master
 *
 * @param  {type} name name of area
 */
function sendArea(name) {
  areas.forEach((item, i) => {
    if (item.name === name) {
      sendCMD("area", "add " + item.surface + " " + item.name + " " + item.x + " " + item.y + " " + (item.x + item.width) + " " + (item.y + item.height));
      return true;
    }
  });
}


/**
 * sendAllAreas - sends all defined areas to pupil master
 *
 */
function sendAllAreas() {
  areas.forEach((item, i) => {
    sendCMD("area", "add " + item.surface + " " + item.name + " " + item.x + " " + item.y + " " + (item.x + item.width) + " " + (item.y + item.height));
  });
}
/**
 * Returns a unique trackId
 * @returns {string} A unique trackId
 */
var getNextId = function () {
    idCounter++;
    return (idCounter + Math.random()) + "";
};
var idCounter = 0;

var {getBasicPropertiesTemplate, getBasicPropertiesTemplateForDB, getPropertiesTemplate, serialize, deserialize, getClassName, instantiateClass} = require('./scheme');

/**
 * This class is for gameobjects that should be tracked and saved in DB when
 * gazed at. It gets a createJsObject and syncProperties like defined in scheme.js
 */
class TrackedObject {
  constructor(createJsObject, syncProperties = getPropertiesTemplate(), isHelper = false, postponeInit = false, id) {
    id = id || getNextId();
    this.createJsObject = createJsObject;
    this.syncProperties = syncProperties;
    this.trackId = id;
    this.createJsObject.trackId = id;

    /** List of TrackIds, that are used as a helper
     * @type {Set.<string>} */
    this.helpers = new Set();

    /** Is this TrackedObject created to represent the property of another TrackedObject
     * @type {boolean} */
    this.isHelper = isHelper === true ? true : false;

    this.last_seen = null;

    trackedObjects.set(id, this);
  }

  delete() {
    trackedObjects.delete(this.trackId);
  }

  /**
   * Returns a sendable representation of this TrackedObject, but helpers are just referenced and not completely included in the return value.
   * This method is for internal use. Use toSendables to serialize a SharedObject for transmission.
   * @returns {Sendable} A sendable representation of this SharedObject.
   */
  sendable () {
      // Store general information
      var res = {
          createJsObject: {}, // Contains the values of all syncedProperties
          trackId: this.trackId, // The trackId of this TrackedObject
          isHelper: this.isHelper, // Is this TrackedObject created to represent the property of another TrackedObject
          syncProperties: this.syncProperties, // Defines which properties sync their changes with the server
          className: getClassName(this.createJsObject) // The class name of the createJsObject
      }

      // Store parent
      if (this.createJsObject.parent && this.createJsObject.parent.trackId) res.parent = this.createJsObject.parent.trackId;
      else {
          switch (this.createJsObject.parent) {
              case MTLG.getStage():
                  res.parent = "Stage";
                  break;
              case MTLG.getStageContainer():
                  res.parent = "StageContainer";
                  this.parent = "StageContainer";
                  break;
              case undefined:
              case null:
                  res.parent = "None";
                  break;
              default:
                  MTLG.warn("eyetracking", "TrackedObject.sendable: No behavior defined for parent", this.createJsObject.parent);
                  res.parent = "None";
                  break;
          }
      }

      // Store properties
      for (var p in this.syncProperties) {
          if (this.createJsObject.hasOwnProperty(p)) {
              res.createJsObject[p] = serialize(this.createJsObject[p], this.syncProperties[p], this);
          }
      }

      return res;
  }

  /**
   * Returns an associative Array containing the sendable versions (serializable) of this TrackedObject and all helpers needed to recreate this TrackedObject.
   * Use this method to transmit a SharedObject.
   * @returns {Object.<string, Sendable>} The associative Array containing the sendables of this TrackedObject and all helpers needed to recreate this TrackedObject.
   */
  toSendables () {
      //TODO muss das gemacht werden? garbageCollectHelpers(); // Remove all unneeded helpers from the helpers list
      var res = {}; // Associative Array containing the sendables of the given TrackedObject and all helpers
      // Convert sharedObject to sendable
      res[this.trackId] = this.sendable();
      res[this.trackId].isRoot = true; // Mark the root of the sendables, the others are only helpers
      // Convert helpers to sendable and add to list
      var helpers = new Set(this.helpers); // var helpers contains the ids of TrackedObject, that are still to convert
      while (helpers.size !== 0) { // While there are still helpers to convert,
          for (let helperId of helpers) { // iterate over them and convert
              if (!res[helperId]) { // Helper is not already converted
                  var helper = trackedObjects.get(helperId); // Get the TrackedObject from the sharedId
                  res[helperId] = helper.sendable(); // Convert to sendable and add to list
                  for (let i of helper.helpers) { // Add the helpers of the helper to the list of TrackedObject to convert,
                      if (!res[i]) { // if they are not already converted
                          helpers.add(i);
                      }
                  }
              }
              helpers.delete(helperId); // Remove converted helper from the list
          }
      }
      return res;
  }
}

/**
 * createAreaWithTags - This functions creates an area like createArea and also adds apriltags in each corner
 *
 * @param  {string} name      name of the area. Must be one word like "searchingarea" or "searching_area"
 * @param  {number} x         x coordinate of the beginning of the area
 * @param  {number} y         y coordinate of the beginning of the area
 * @param  {number} width     width of the area
 * @param  {number} height    height of the area
 * @param  {gameObject} stage stage where to add the tags
 * @param  {object} options   This option object can specify the width and height of the apriltags
 */
function createAreaWithTags(name, x, y, width, height, stage, options) {
  surfaces.push({ name: name, x: x, y: y, width: width, height: height });

  // send area to pupils core master
  if (connected) {
    sendSurface(name);
  }
  // create for differen tags for each corner one
  var tagid1 = getUnusedTag();
  var tagid2 = getUnusedTag();
  var tagid3 = getUnusedTag();
  var tagid4 = getUnusedTag();
  var tagid5 = getUnusedTag();
  var tagid6 = getUnusedTag();
  var tagid7 = getUnusedTag();
  var tagid8 = getUnusedTag();

  var defaultOptions = {
    width: defaults.width,
    height: defaults.height
  }
  defaultOptions = Object.assign(defaultOptions, options);

  var tag1 = MTLG.utils.apriltag.getApriltag(defaults.tagfamiliy, tagid1, defaultOptions);
  tag1.x = x;
  tag1.y = y;
  stage.addChild(tag1);

  var tag2 = MTLG.utils.apriltag.getApriltag(defaults.tagfamiliy, tagid2, defaultOptions);
  tag2.x = width - defaultOptions.width;
  tag2.y = y;
  stage.addChild(tag2);

  var tag3 = MTLG.utils.apriltag.getApriltag(defaults.tagfamiliy, tagid3, defaultOptions);
  tag3.x = x;
  tag3.y = height - defaultOptions.height;
  stage.addChild(tag3);

  var tag4 = MTLG.utils.apriltag.getApriltag(defaults.tagfamiliy, tagid4, defaultOptions);
  tag4.x = width - defaultOptions.width;
  tag4.y = height - defaultOptions.height;
  stage.addChild(tag4);

  var tag5 = MTLG.utils.apriltag.getApriltag(defaults.tagfamiliy, tagid5, defaultOptions);
  tag5.x = width / 2 - defaultOptions.width * 1 / 2;
  tag5.y = 0;
  stage.addChild(tag5);

  var tag6 = MTLG.utils.apriltag.getApriltag(defaults.tagfamiliy, tagid6, defaultOptions);
  tag6.x = width / 2 - defaultOptions.width * 1 / 2;
  tag6.y = height - defaultOptions.height;
  stage.addChild(tag6);

  var tag7 = MTLG.utils.apriltag.getApriltag(defaults.tagfamiliy, tagid7, defaultOptions);
  tag7.x = 0;
  tag7.y = height / 2 - defaultOptions.height/2;
  stage.addChild(tag7);

  var tag8 = MTLG.utils.apriltag.getApriltag(defaults.tagfamiliy, tagid8, defaultOptions);
  tag8.x = width  - defaultOptions.width;
  tag8.y = height / 2 - defaultOptions.height/2;
  stage.addChild(tag8);
}



/**
 * createArea - Creates an area for tracking. If connected to pupil core it automatically sends the area.
 *
 * @param  {type} name   name of the area. Must be one word like "searchingarea" or "searching_area"
 * @param  {type} x      x coordinate of the beginning of the area
 * @param  {type} y      y coordinate of the beginning of the area
 * @param  {type} width  width of the area
 * @param  {type} height height of the area
 */
function createArea(name, x, y, width, height, surface = null) {
  if (surface == null) {
    surface = "surface";
  }
  areas.push({ name: name, x: x, y: y, width: width, height: height, surface: surface });

  // send area to pupils core master
  if (connected) {
    sendArea(name);
  }
}


/**
 * getUnusedTag - Helperfunction to get a new apriltag
 *
 * @return {type}  returns tagid of an unused apriltag
 */
function getUnusedTag() {
  var tagid = 0;
  var countOfTags = MTLG.utils.apriltag.getFamilies()[defaults.tagfamiliy].count;
  while (usedTags.includes(tagid)) {
    tagid++;
    if (tagid >= countOfTags) {
      return null;
    }
  }
  usedTags.push(tagid);
  return tagid;
}


/**
 * detectCollisionsWithAreas - This function detects a collision of the given
 * coordinate with any defined area. The coordinate is handled like a square
 * with the size defined in defaults.gazePrecision
 *
 * @param  {type} coord Object like {x: 0, y: 0}
 * @return {type}       description
 */
function detectCollisionsWithAreas(coord) {
  var ret = [];
  areas.forEach((area, i) => {
    if (MTLG.utils.collision.calculateIntersection(area, { x: coord.x, y: coord.y, width: defaults.gazePrecision, height: defaults.gazePrecision })) {
      ret.push(area.name);
    }
  });
  return ret;
}

/**
 * detectCollisions - This is a helper function to detect if a
 * gaze collides with a gameObject that is tracked
 *
 * @param  {object} coord  Object like {x: 0, y: 0}
 * @return {array} If the coordinates are on a gameObject, it will be returned in an array otherwise an empty array.
 */
function detectCollisions(coord) {
  var objects = trackedObjects;
  var ret = [];
	objects.forEach((obj, i) => {
    if(obj.createJsObject.parent === null) {
      obj.delete();
    } else {
      let collision = detectCollisionWithGameObject(obj.createJsObject, coord);

  		if(collision != null) {
  		  ret.push(obj);
  		}
    }
	});
  return ret;
}

/**
 * detectAllCollisions - This function detects collision with all gameObjects on
 * the stage recursively.
 *
 * @param  {object} coord  Object like {x: 0, y: 0}
 * @return {array} If the coordinates are on a gameObject, it will be returned in an array otherwise an empty array.
 */
function detectAllCollisions(coord) {
  var objects = MTLG.getStageContainer().children;
  return detectCollisionRec(coord,objects);
}


/**
 * detectCollisionRec - Helperfunction for recursion
 *
 * @param  {type} coord   description
 * @param  {type} objects description
 * @return {type}         description
 */
function detectCollisionRec(coord, objects){
	var ret = [];
	objects.forEach((obj, i) => {
		let collision = detectCollisionWithGameObject(obj, coord);

		if(collision != null) {
		  ret.push(obj);
		}
		if (obj.children) {
			ret = ret.concat(detectCollisionRec(coord,obj.children));
		}
	});
  return ret;
}


/**
 * detectCollisionWithGameObject - This function detects a collsion of the
 * coordinate coord with the given gameObject. The coordinate is handles as
 * a square with the size of defaults.gazePrecision. In order to detect a
 * collision the gameObject must have bounds set.
 *
 * @param  {type} gameObject description
 * @param  {type} coord      description
 * @return {type}            description
 */
function detectCollisionWithGameObject(gameObject, coord) {
  let localToGlobalCoord = gameObject.localToGlobal(0,0)
  localToGlobalCoord.x = localToGlobalCoord.x / MTLG.getStageContainer().scaleX;
  localToGlobalCoord.y = localToGlobalCoord.y / MTLG.getStageContainer().scaleY;
  let bounds = gameObject.getBounds();
  if(!bounds || (!bounds.x && bounds.x !== 0) || (!bounds.y && bounds.y !== 0)) {
	   return null;
  }
  bounds.x = bounds.x + localToGlobalCoord.x;
  bounds.y = bounds.y + localToGlobalCoord.y;

  return MTLG.utils.collision.calculateIntersection(bounds, {x: coord.x - defaults.gazePrecision * 1/2, y: coord.y - defaults.gazePrecision * 1/2, width: defaults.gazePrecision, height: defaults.gazePrecision});
}


/**
 * getGazeOfPlayer - For detecting gameObjects, the bounds of a gameObject must be set.
 *
 * @param  {type} playerID description
 * @return {object}        returns an object containing coord and gameObjects as array
 */
function getGazeOfPlayer(playerID) {
  var ret = {};
  ret.coord = getGazeCoordinatesOfPlayer(playerID);
  ret.gameObjects = [];
  if (ret.coord != null) {
    // check if collides with gameObjects
    var gameObjects = detectCollisions(ret.coord);
    if (gameObjects.length > 0) {
      ret.gameObjects = gameObjects;
    }
  }
  return ret;
}


/**
 * getGazeCoordinatesOfPlayer - returns the coordniates of a player's last gaze
 *
 * @param  {type} playerID MTLG id of player
 * @return {type}          coordinates
 */
function getGazeCoordinatesOfPlayer(playerID) {
  var id = playersIDs.get(playerID);
  return players.get(id).coord;
}


/**
 * isObjectWatched - checks if the given object was watched and if given if it
 * was watched by a specific player. If the gameObject given is not a
 * TrackedObject it is checked by the players' last gaze.
 *
 *
 * @param  {type} gameObject      description
 * @param  {type} playerID = null description
 * @return {type}                 description
 */
function isObjectWatched(gameObject, playerID = null) {
  if(gameObject.trackId) {
    var trackObj = trackedObjects.get(gameObject.trackId);
    var id = null;
    if(playerID != null) id = playersIDs.get(playerID);
    if(trackObj.last_seen != null && (playerID == null || id == trackObj.last_seen.player)) {
      return trackObj.last_seen;
    }
    return false;
  }
  if(player != null) {
    return detectCollisionWithGameObject(gameObject, getGazeCoordinatesOfPlayer(playerID));
  }
  return false;
}


/**
 * getAllWatchedObjects - gives all TrackedObjects that were watched at any time
 * in game.
 *
 * @return {[TrackedObject]}  array of TrackedObjects.
 */
function getAllWatchedObjects() {
  var ret = [];

  trackedObjects.forEach((obj) => {
    if(obj.last_seen != null) {
      ret.push(obj)
    }
  });

  return ret;
}


/**
 * getGazes - gives the current gaze of all players.
 *
 * @return {type}  array of coordinates.
 */
function getGazes() {
  var ret = [];
  if (playersInitialized) {
    var numberOfPlayer = MTLG.getPlayerNumber();
    for (var i = 0; i < numberOfPlayer; i++) {
      var gaze = getGazeOfPlayer(i);
      ret.push(gaze);
    }
  }
  return ret;
}


/**
 * subscribeToAllEvents - With this function a listener get subscribed to all
 * events meaning whenever a gaze event is sent from the pupil glasses the
 * listener gets calld.
 *
 * @param  {type} listener function that should get called.
 * @return {type}          returns the id of the listener, to later remove it.
 */
function subscribeToAllEvents(listener) {
  listenerAllEventsID++;
  listenersForAllEvents.push({
    id: listenerAllEventsID,
    callback: listener
  });
  return listenerAllEventsID;
}


/**
 * createListener - Helperfunction to create standard listener
 *
 * @param  {type} listener        description
 * @param  {type} id              description
 * @param  {type} min_duration    description
 * @param  {type} update_interval description
 * @param  {type} obj = null      description
 * @param  {type} area = null     description
 * @return {type}                 description
 */
function createListener(listener, id, min_duration, update_interval, obj = null, area = null) {
  return {
    id: id,
    object: obj,
    area: area,
    callback: listener,
    min_duration: min_duration,
    update_interval: update_interval,
    fixation_duration: new Map(),
    next_fixation_at: new Map(),
    last_visit: new Map(),
  };
}


/**
 * registerListenerForObject - When registering for an object, everytime when
 * the object is watched by a player the listener is called. With min_duration
 * the listener gets only called, when the gaze is fixed on the object for at
 * least the amount of min_duration. When update_interval is set, then the
 * listener gets only called again after this interval (if it is set to 0 the
   * listener gets called until the gaze is no longer on the object)
 *
 * @param  {type} obj                 createJS GameObject or TrackedObject
 * @param  {type} listener            callback function
 * @param  {type} min_duration = 0    minimal time the gaze has to be on the object
 * @param  {type} update_interval = 0 interval for next call
 * watched
 * @return {type}                     id of the listener for later removal
 */
function registerListenerForObject(obj, listener, min_duration = 0, update_interval = 0) {
  if(!(obj instanceof TrackedObject)) {
    obj = new TrackedObject(obj, getBasicPropertiesTemplateForDB());
  }
  listenerForObjectID++;
  listenersForObjects.set(obj.trackId, createListener(listener, listenerForObjectID, min_duration, update_interval, obj, null));
  return listenerForObjectID;
}

/**
 * registerListenerForAllObjects - listener gets called whenever any object is
 * watched. With min_duration the listener gets only called, when the gaze is
 * fixed on the object for at least the amount of min_duration. When
 * update_interval is set, then the listener gets only called again after this
 * interval (if it is set to 0 the listener gets called until the gaze is no
 * longer on the object)
 *
 * @param  {type} listener            callback function
 * @param  {type} min_duration = 0    minimal time the gaze has to be on the object
 * @param  {type} update_interval = 0 interval for next call
 * @return {type}                     id of the listener for later removal
 */
function registerListenerForAllObjects(listener, min_duration = 0, update_interval = 0) {
  listenerForAllObjectsID++;
  listenersForAllObjects.push(createListener(listener, listenerForAllObjectsID, min_duration, update_interval));
  return listenerForAllObjectsID;
}

/**
 * registerListenerForArea - The listener gets called whenever someone looks
 * inside the area. With min_duration the listener gets only called, when the
 * gaze is iside the area for at least the amount of min_duration. When
 * update_interval is set, then the listener gets only called again after this
 * interval (if it is set to 0 the listener gets called until the gaze is no
 * longer on the object)
 *
 * @param  {type} area                name of the area
 * @param  {type} listener            callback function
 * @param  {type} min_duration = 0    minimal time the gaze has to be on the object
 * @param  {type} update_interval = 0 interval for next call
 * @return {type}                     id of the listener for later removal
 */
function registerListenerForArea(area, listener, min_duration = 0, update_interval = 0) {
  listenerForAreaID++;
  listenersForAreas.push(createListener(listener, listenerForAreaID, min_duration, update_interval, null, area));
  return listenerForAreaID;
}


/**
 * shouldTriggerListener - Helperfunction
 *
 */
function shouldTriggerListener(listener, event) {
  if (!listener.fixation_duration.has(event.player)) {
    listener.fixation_duration.set(event.player, 0);
  }
  if (!listener.next_fixation_at.has(event.player)) {
    listener.next_fixation_at.set(event.player, 0);
  }

  let eventIsLongEnough = listener.min_duration == 0 ? true : false;

  let fixation_cooldown = listener.next_fixation_at.get(event.player) - event.time;
  if (fixation_cooldown <= 0 && event.fixations.length > 0) {
    let fixation_duration = listener.fixation_duration.get(event.player);
    let duration = event.fixations[event.fixations.length - 1].duration;
    let total_fixation_duration = fixation_duration + duration;
    listener.fixation_duration.set(event.player, total_fixation_duration);
    if (total_fixation_duration >= listener.min_duration) {
      eventIsLongEnough = true;
    } else {
      let remaining_duration = listener.min_duration - total_fixation_duration;
      let next_cooldown = duration;
      if (next_cooldown > remaining_duration) {
        next_cooldown = remaining_duration;
      }
      // event.time is in seconds, but all other times are in ms
      listener.next_fixation_at.set(event.player, event.time + (next_cooldown / 1000));
    }
  } else if (fixation_cooldown <= 0) {
    // Fixation evaluation is not currently on cooldown, but there is no fixation => reset fixation time
    listener.fixation_duration.set(event.player, 0);
  }

  let updateIntervalPassed = (!listener.last_visit.get(event.player) || listener.last_visit.get(event.player) < Date.now() - listener.update_interval);

  return eventIsLongEnough && updateIntervalPassed;
}


/**
 * removeListenerForObject - Removes the listener of the object
 *
 * @param  {type} object_id Id of the object
 */
function removeListenerForObject(object_id) {
  listenersForObjects.delete(object_id);
}


/**
 * removeListenerForAllObjects - Removes the listener of all objects with the
 * given id
 *
 * @param  {type} id id of the listener
 */
function removeListenerForAllObjects(id) {
  let index = -1;
  for (let i = 0; i < listenersForAllObjects.length; i++) {
    if (listenersForAllObjects[i].id === id) {
      index = i;
      break;
    }
  }
  listenersForAllObjects.splice(index, 1);
}


/**
 * removeListenerForArea - removes the listener of an area with the given id.
 *
 * @param  {type} id id of the listener
 */
function removeListenerForArea(id) {
  let index = -1;
  for (let i = 0; i < listenersForAreas.length; i++) {
    if (listenersForAreas[i].id === id) {
      index = i;
      break;
    }
  }
  listenersForAreas.splice(index, 1);
}


/**
 * unsubscribeFromAllEvents - unsubscribes from all events so that the listener
 * is no longer called.
 *
 * @param  {type} id id of listener
 */
function unsubscribeFromAllEvents(id) {
  let index = -1;
  for (let i = 0; i < listenersForAllEvents.length; i++) {
    if (listenersForAllEvents[i].id === id) {
      index = i;
      break;
    }
  }
  listenersForAllEvents.splice(index, 1);
}


/**
 * visualizeGazeOfPlayer - The gaze of the player is visualized by a filled dot.
 *
 * @param  {type} playerID Id of player
 * @param  {type} color    color of the dot
 */
function visualizeGazeOfPlayer(playerID, color) {

  var gazeObj = new createjs.Shape();
  gazeObj.graphics.beginFill(color).drawCircle(0, 0, 20);
  gazeObj.x = 100;
  gazeObj.y = 100;

  eyetracking_stage.addChild(gazeObj);
  subscribeToAllEvents(function (event) {
	var id = playersIDs.get(playerID);
    if (event.player == id) {
      var screenposition = event.gazes[event.gazes.length - 1].screen_pos;
      var coordinates = {
        x: screenposition[0],
        y: screenposition[1]
      }

      gazeObj.x = screenposition[0];
      gazeObj.y = screenposition[1];
    }
  });
}


/**
 * visualizeGazeOfAllPlayers - Visualizes the gazes of all players with for
 * different colored dots.
 *
 * @return {type}  description
 */
function visualizeGazeOfAllPlayers() {
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    visualizeGazeOfPlayer(i, colors[i]);
  }
}


// injecting into MTLG
MTLG.eyetracking = {
  createSurface: createAreaWithTags,
  createArea: createArea,
  removeArea: removeArea,
  initialize: initialize,
  initPlayers: initPlayers,
  getGazeOfPlayer: getGazeOfPlayer,
  getGazes: getGazes,
  isObjectWatched: isObjectWatched,
  getAllWatchedObjects: getAllWatchedObjects,
  subscribeToAllEvents: subscribeToAllEvents,
  registerListenerForObject: registerListenerForObject,
  registerListenerForAllObjects: registerListenerForAllObjects,
  registerListenerForArea: registerListenerForArea,
  removeListenerForObject: removeListenerForObject,
  removeListenerForAllObjects: removeListenerForAllObjects,
  removeListenerForArea: removeListenerForArea,
  unsubscribeFromAllEvents: unsubscribeFromAllEvents,
  TrackedObject: TrackedObject,
  visualizeGazeOfPlayer: visualizeGazeOfPlayer,
  visualizeGazeOfAllPlayers: visualizeGazeOfAllPlayers,
  trackedObjects: trackedObjects
};
MTLG.addModule(init);
